var app = angular.module('myApp', ['emailParser']);

app.controller('MyController', ['$scope', 'EmailParser', function($scope, EmailParser) {
	$scope.$watch('emailBody', function(body) {
		if (body) {
			$scope.previewText = EmailParser.parse(body, {
				to: $scope.to
			});
		}
	});
}]);

angular.module('emailParser', [])
	.config(['$interpolateProvider', function($interpolateProvider) {
		$interpolateProvider.startSymbol('[');
		$interpolateProvider.endSymbol(']');
	}])
	.factory('EmailParser', ['$interpolate', function($interpolate) {
		return {
			parse: function(text, context) {
				var template = $interpolate(text);
				return template(context);
			}
		}
	}]);